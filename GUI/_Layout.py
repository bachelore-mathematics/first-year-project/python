import tkinter as tk
import tkinter.ttk as ttk
import tkinter.scrolledtext as tkst
from os import listdir, mkdir
from glob import glob
from os.path import isfile, join, isdir
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import json
import time


# build_gui 
#
# Description
# Build the graphical interface. 
#
# Input : None
# Output : None
# Catch : None
def build_gui(self):
    # Create the overall Layout
    self.Master = tk.Tk()
    self.Master.title("The awesome particle mass calculator")


    self.header = tk.Frame(self.Master)
    self.header.grid(column=0,row=0)

    self.middle = tk.Frame(self.Master)
    self.middle.grid(column=0,row=1)

    self.footer = tk.Frame(self.Master)
    self.footer.grid(column=0,row=2)



    self.left = tk.Frame(self.middle)
    self.left.grid(column=0,row=0)

    self.right = tk.Frame(self.middle)
    self.right.grid(column=1,row=0)

    # Footer layout
    self.error = tk.Frame(self.footer)
    self.error.grid(column=0,row=1)

    self.btn = tk.Frame(self.footer)
    self.btn.grid(column=0,row=0)

    # Left layout this include, file selector, table overview, particle overview
    self.file = tk.Frame(self.header)
    self.file.grid(column=0,row=0)

    self.particle = tk.Frame(self.header)
    self.particle.grid(column=0,row=1)

    self.table = tk.Frame(self.left)
    self.table.grid(column=0,row=2)

    # Right layout this include, Graph, graph selecter, loaded file selector
    self.gsel = tk.Frame(self.header)
    self.gsel.grid(column=1,row=0)

    self.graph = tk.Frame(self.right)
    self.graph.grid(column=0,row=1)

    self.lfile = tk.Frame(self.btn)
    self.lfile.grid(column=1,row=0)

    # Display and show error
    self.error_label =  tk.Label(self.error,text="Errors")
    self.error_label.grid(column=1,row=0)

    self.error_text = tkst.ScrolledText(self.error, bg = "white", fg="black")
    self.error_text.grid(column=1,row=1)
    
    self.btn_convert_latex = tk.Button(self.btn,text="Convert to Latex",command=self.convert_latex)
    self.btn_convert_latex.grid(column=0,row=0)

    # File selector
    # First Grab all the files within the filestytem
    files = [f for f in listdir("datafiles/") if isfile(join("datafiles/", f))]
    files = sorted(files)
    self.variable = tk.StringVar(self.file)
    self.variable.set(files[0])
    self.filename = self.variable.get()
    # Create and place the file selector option. It is indeed a dropdown menu
    self.fileselector = ttk.Combobox(self.file,textvariable=self.variable,values=files,width=30)
    self.fileselector.grid(column=0,row=0)
    # Add a button to make it possible to select. 
    self.filebutton = tk.Button(self.file,text="Select",command=self.get_variable,height=1)
    self.filebutton.grid(column=1,row=0)


    # Table particle information 
    # Extract the different information from the filename
    self.particlename = tk.StringVar()
    self.quarksname = tk.StringVar()
    particlename_index_start = self.filename.find(".")
    particlename_index_end = self.filename.find(".",particlename_index_start+1)
    self.particlename.set(self.filename[particlename_index_start+1:particlename_index_end])
    quarks_index = self.filename.rfind(".",-5)
    self.quarksname.set(self.filename[quarks_index+1:quarks_index+6])
    # Create the table
    self.particle_name = tk.Label(self.particle, text = "Particle")
    self.particle_name.grid(column=0,row=0)
    self.particle_name_text = tk.Label(self.particle, textvariable=self.particlename)
    self.particle_name_text.grid(column=1,row=0)

    self.quarks = tk.Label(self.particle,text="Quarks")
    self.quarks.grid(column=2,row=0)
    self.quarks_text = tk.Label(self.particle,textvariable=self.quarksname)
    self.quarks_text.grid(column=3,row=0)


    # Calculated information about the system
    # Prepare the start values
    self.stat_chi_text = tk.StringVar()
    self.stat_chi_norm_text = tk.StringVar()
    self.stat_chi_red_text = tk.StringVar()
    self.stat_sigma_data_text = tk.StringVar()
    self.m1_value = tk.StringVar()
    self.m1_sigma = tk.StringVar()
    self.m2_value = tk.StringVar()
    self.m2_sigma = tk.StringVar()
    self.z1_value = tk.StringVar()
    self.z1_sigma = tk.StringVar()
    self.z2_value = tk.StringVar()
    self.z2_sigma = tk.StringVar()

    self.stat_chi_text.set(0)
    self.stat_chi_norm_text.set(0)
    self.stat_chi_red_text.set(0)
    self.stat_sigma_data_text.set(0)
    self.m1_value.set(0)
    self.m1_sigma.set(0)
    self.m2_value.set(0)
    self.m2_sigma.set(0)
    self.z1_value.set(0)
    self.z1_sigma.set(0)
    self.z2_value.set(0)
    self.z2_sigma.set(0)

    # Create the a table of information
    self.stat_chi = tk.Label(self.table,text="Chi Squared")
    self.stat_chi.grid(column=0,row=1)
    self_stat_chi_value = tk.Label(self.table,textvariable=self.stat_chi_text)
    self_stat_chi_value.grid(column=1,row=1)

    self.stat_chi_norm = tk.Label(self.table,text="Normalized Chi")
    self.stat_chi_norm.grid(column=0,row=2)
    self.stat_chi_norm_value = tk.Label(self.table,textvariable=self.stat_chi_norm_text)
    self.stat_chi_norm_value.grid(column=1,row=2)

    self.stat_chi_red = tk.Label(self.table,text="Reduced Chi")
    self.stat_chi_red.grid(column=0,row = 3)
    self.stat_chi_red_value = tk.Label(self.table,textvariable=self.stat_chi_red_text)
    self.stat_chi_red_value.grid(column=1,row=3)

    self.stat_sigma_data = tk.Label(self.table,text="Standard Deviation")
    self.stat_sigma_data.grid(column=0,row=4)
    self.stat_sigma_data_value = tk.Label(self.table,textvariable=self.stat_sigma_data_text)
    self.stat_sigma_data_value.grid(column=1,row=4)

    self.est_value = tk.Label(self.table,text="Estimate")
    self.est_value.grid(column=3,row=0)
    self.est_sigma = tk.Label(self.table,text="Standard Deviation")
    self.est_sigma.grid(column=4,row=0)

    self.m1 = tk.Label(self.table,text="m1")
    self.m1.grid(column=2,row=1)
    self.m1_value_label = tk.Label(self.table,textvariable=self.m1_value)
    self.m1_sigma_label = tk.Label(self.table,textvariable=self.m1_sigma)
    self.m1_value_label.grid(column=3,row=1)
    self.m1_sigma_label.grid(column=4,row=1)

    self.z1 = tk.Label(self.table,text="z1")
    self.z1.grid(column=2,row=2)
    self.z1_value_label = tk.Label(self.table,textvariable=self.z1_value)
    self.z1_sigma_label = tk.Label(self.table,textvariable=self.z1_sigma)
    self.z1_value_label.grid(column=3,row=2)
    self.z1_sigma_label.grid(column=4,row=2)

    self.m2 = tk.Label(self.table,text="m2")
    self.m2.grid(column=2,row=3)
    self.m2_value_label = tk.Label(self.table,textvariable=self.m2_value)
    self.m2_sigma_label = tk.Label(self.table,textvariable=self.m2_sigma)
    self.m2_value_label.grid(column=3,row=3)
    self.m2_sigma_label.grid(column=4,row=3)

    self.z2 = tk.Label(self.table,text="z2")
    self.z2.grid(column=2,row=4)
    self.z2_value_label = tk.Label(self.table,textvariable=self.z2_value)
    self.z2_sigma_label = tk.Label(self.table,textvariable=self.z2_sigma)
    self.z2_value_label.grid(column=3,row=4)
    self.z2_sigma_label.grid(column=4,row=4)


    # Update the variables with information

    # Right layout this include, Graph, graph selecter, loaded file selector
    cmn_width = 15
    self.label_start = tk.Label(self.gsel,text="Fitting Start")
    self.label_start.grid(column=0,row=0)
    self.entry_start = tk.Entry(self.gsel)
    self.entry_start.grid(column=1,row=0)
    self.label_end = tk.Label(self.gsel,text="Fitting End")
    self.label_end.grid(column=0,row=1)
    self.entry_end = tk.Entry(self.gsel)
    self.entry_end.grid(column=1,row=1)

    self.btn_fit = tk.Button(self.gsel,text="Add Fit", command=self.add_fit)
    self.btn_fit.grid(column=2,row=1)

    self.btn_sigma = tk.Button(self.gsel,text="Show sigma",width=cmn_width, command=self.show_sigma)
    self.btn_sigma.grid(column=3,row=0)

    self.btn_curvefit = tk.Button(self.gsel,text="Show Curve fit",width=cmn_width, command=self.show_curve_fit)
    self.btn_curvefit.grid(column=3,row=1)

    self.btn_rawdata = tk.Button(self.gsel,text="Show Raw data",width=cmn_width,command=self.show_raw_data)
    self.btn_rawdata.grid(column=4,row=0)

    self.btn_resample = tk.Button(self.gsel,text="Show Resample data",width=cmn_width,command=self.show_resample)
    self.btn_resample.grid(column=4,row=1)

    self.btn_effective = tk.Button(self.gsel, text = "Show Effective mass", width=cmn_width,command=self.show_effective)
    self.btn_effective.grid(column=5,row=0)

    self.btn_effective_error = tk.Button(self.gsel, text="Sigma Effective mass", width=cmn_width, command=self.show_effective_error)
    self.btn_effective_error.grid(column=5,row=1)

    #self.graph
    self.data = dict()


    self.figure = plt.Figure(figsize=(6,5), dpi=100)
    self.ax = self.figure.add_subplot(111)
    self.chart_type = FigureCanvasTkAgg(self.figure, self.graph)
    self.chart_type.get_tk_widget().grid(column=0,row=0)
    self.chart_type.draw()


    #self.lfile
    self.loaded_file = glob("Gen*")
    self.loaded_file_fit = ["None"]
    if not self.loaded_file:
        self.loaded_file = ["Nonne"]
    else:
        if glob(self.loaded_file[0] + "/*"):
            self.loaded_file_fit = listdir(self.loaded_file[0])
    self.curr_fit = tk.StringVar()
    self.curr_fit.set(self.loaded_file_fit[0])

    self.curr_file = tk.StringVar()
    self.curr_file.set(self.loaded_file[0])

    self.loadfile = ttk.Combobox(self.lfile,textvariable=self.curr_file,values=self.loaded_file,width=30)
    self.loadfile.bind('<<ComboboxSelected>>',self.update_file_fit)
    self.loadfile.grid(column=0,row=0)

    self.loadfile_fit = ttk.Combobox(self.lfile,textvariable=self.curr_fit,values=self.loaded_file_fit,width=15)
    self.loadfile_fit.grid(column=1,row=0)


    self.btn_loadfile = tk.Button(self.lfile,text="Select File",command=self.select_file)
    self.btn_loadfile.grid(column=2,row=0)

    self.Master.after(200,self.check_update)

    self.Master.mainloop()


# update_file_fit 
#
# Description
# Update the combobox with file fit
#
# Author
# Stefan Quvang Christensen 06/05/2021
#
# Input : self
# Output : none
# Catch : none
def update_file_fit(self,event):
    self.loaded_file_fit = listdir(self.curr_file.get())
    self.loadfile_fit['values'] = self.loaded_file_fit
    self.curr_fit.set(self.loaded_file_fit[0])


# convert_latex 
#
# Description
# Convert chosen graph to latex code
#
# Author
# Stefan Quvang Christensen 27/04/2021
#
# Input : self
# Output : None
# Catch : None
def convert_latex(self):
    window_latex = tk.Toplevel(self.Master)
    window_latex.title("Latex Converter")

    # Create some layout to this window
    frame_select_graph = tk.Frame(window_latex)
    frame_select_graph.grid(column=0,row=0)

    self.frame_combine_graph = tk.Frame(window_latex)
    self.frame_combine_graph.grid(column=1, row=0)

    frame_convert = tk.Frame(window_latex)
    frame_convert.grid(column=0, row=1)

    frame_cancel = tk.Frame(window_latex)
    frame_cancel.grid(column=1,row=1)

    frame_copy = tk.Frame(window_latex)
    frame_copy.grid(column=2,row=1)


    # Fill in the layout
    # The "TOP" frame containing the graph selector

    self.dict_radio_graph = dict()
    self.dict_checkbox_val = dict()
    self.list_graph = ["Curve fit","Effective Mass","Error bar","Jackknife","Raw Data","Sigma meff"]
    self.dict_label_obj = dict()
    self.dict_multiplot_obj = dict()
    self.dict_multiplot_val = dict()
    co = 0
    ro = 0
    for key in self.list_graph:
        self.dict_checkbox_val[key] = tk.BooleanVar() # tk.IntVar()
        self.dict_radio_graph[key] = tk.Checkbutton(frame_select_graph,text=key,variable=self.dict_checkbox_val[key],command=self.add_option)
        self.dict_radio_graph[key].grid(column=co,row=ro)
        ro = ro + 1
    # The "BUTTOM" frame containing the buttons. 
    btn_convert = tk.Button(frame_convert,text="Convert", command=self.convert_data)
    btn_convert.grid(column=0,row=0)

    btn_cancel = tk.Button(frame_cancel,text="Cancel", command=window_latex.destroy)
    btn_cancel.grid(column=0,row=0)

    self.var_copy = tk.Variable()
    label = tk.Label(frame_copy, textvariable=self.var_copy)
    label.grid(column=0,row=0)


# add_option 
#
# Description
# callback function for toggling labels
#
# Author
# Stefan Quvang Christensen
#
# Input : self
# Output : non
# Catch : no
def add_option(self):
    for key in self.dict_checkbox_val.keys():
        if self.dict_checkbox_val[key].get() and key not in self.dict_label_obj:
            co = 0
            inc = int(round(len(self.list_graph) )/2)
            ro = self.list_graph.index(key) * inc

            self.dict_label_obj[key] = tk.Label(self.frame_combine_graph,text=key)
            self.dict_label_obj[key].grid(column=co,row=ro)

            tmp_dict = dict()
            tmp_dict_val = dict()
            col = 1
            row = ro
            for graph in self.list_graph:
                if graph != key:
                    tmp_dict_val[graph] = tk.BooleanVar()
                    tmp_dict[graph] = tk.Checkbutton(self.frame_combine_graph,text=graph,variable=tmp_dict_val[graph])
                    tmp_dict[graph].grid(column=col,row=row)
                    if col == 2:
                        col = 1
                        row = row + 1
                    else :
                        col = 2
            self.dict_multiplot_val[key] = tmp_dict_val
            self.dict_multiplot_obj[key] = tmp_dict
        elif key in self.dict_label_obj and not self.dict_checkbox_val[key].get():
            self.dict_label_obj[key].destroy()
            for obj in self.dict_multiplot_obj[key].keys():
                self.dict_multiplot_obj[key][obj].destroy()
            del self.dict_multiplot_obj[key]
            del self.dict_label_obj[key]


# convert_data 
#
# Description
# Prepare data for converting to latex
#
# Author
# Stefan Quvang Christensen 28-04-2021
#
# Input : self
# Output : non
# Catch : non
def convert_data(self):
    # Create dict with the following format
    # {plot1:[plot1,subplot11,subplot12],
    #  plot2:[plot2,subplot21, subplot22]}
    dict_plot = dict()
    for plot in self.dict_label_obj.keys():
        list_tmp = [plot]
        for subplot in self.dict_multiplot_val[plot]:
            if self.dict_multiplot_val[plot][subplot].get():
                list_tmp.append(subplot)
        dict_plot[plot] = list_tmp
    fit = self.curr_fit.get()
    start = fit[0:fit.find("-")]
    end = fit[fit.find("-")+1:len(fit)]
    dict_latex = {  "graph":dict_plot,
                    "file":self.curr_file.get(),
                    "fit":{"start":start,"end":end}
                    }
    self.q_latex.put(dict_latex)


# show_effective 
#
# Description
# Toggle the effective mass
#
# Author
# Stefan Quvang Christensen 24/04/2021
#
# Input : self
# Output : None
# Catch : None
def show_effective(self):
    label = "Effective Mass"
    if self.b_effective == False:
        self.b_effective = True
        self.btn_effective.configure(bg = "green")
        self.effective_plot.set_visible(self.b_effective)
        self.m1_plot.set_visible(self.b_effective)
    elif self.b_effective == True:
        self.b_effective = False
        self.btn_effective.configure(bg="red")
        self.effective_plot.set_visible(self.b_effective)
        self.m1_plot.set_visible(self.b_effective)
    else:
        self.b_effective = True
        self.btn_effective.configure(bg="green")
        x = self.data[label]["x"]
        y = self.data[label]["y"]
        m1 = self.data[label]["m1"]
        self.effective_plot,  = self.ax.plot(x,y,"ro")
        self.m1_plot, = self.ax.plot(x,m1,"b-")
    self.chart_type.draw()


# show_effective_error 
#
# Description
# Toggle the sigma values on/off
#
# Author
# Stefan Quvang Christensen 23/04/2021
#
# Input : self
# Output : None
# Catch : None
def show_effective_error(self):
    label="Sigma meff"
    if self.b_effective_error == False:
        self.b_effective_error = True
        self.btn_effective_error.configure(bg="green")
        for  plot in self.effective_error_plot:
            plot.set_visible(self.b_effective_error)
    elif self.b_effective_error == True :
        self.b_effective_error = False
        self.btn_effective_error.configure(bg="red")
        for plot in self.effective_error_plot:
            plot.set_visible(self.b_effective_error)
    else:
        self.b_effective_error = True
        self.btn_effective_error.configure(bg="green")
        x = self.data[label]["x"]
        y = self.data[label]["y"]
        std = self.data[label]["std"]
        plot = self.ax.errorbar(x=x,y=y,yerr=std,capsize=5)
        plot[0].set_visible(False)
        self.effective_error_plot = plot[1] + plot[2]
    self.chart_type.draw()


# select_file 
#
# Description
# Select a preloaded file and all its data
#
# Author
# Stefan Quvang Christensen
#
# Input : self
# Output : None
# Catch : None
def select_file(self):
    filename = self.loadfile.get()
    string_fit = self.curr_fit.get()
    string_path = filename + "/" + string_fit
    data = dict()
    cal_data = dict()
    files = [f for f in listdir(string_path + "/") if isfile(join(string_path + "/", f))]
    for File in files:
        with open(string_path + "/" + File , "r" ) as f:
            if File == "Calculated data":
                cal_data[File] = json.load(f)
            else:
                data[File] = json.load(f)
    list_data = [0]*len(cal_data["Calculated data"])
    i = 0
    for  key in cal_data["Calculated data"]:
         list_data[i] = cal_data["Calculated data"][key]
         i = i +1
    self.q_variables.put(list_data)
    self.q_data.put(data)
    self.b_curve_fit = ""
    self.b_sigma = ""
    self.b_raw_data = ""
    self.b_resample = ""
    self.b_effective = ""
    self.b_effective_error = ""


# show_raw_data 
#
# Description
# Toggle the raw_data
#
# Author
# Stefan Quvang Christensen 22/04/2021
#
# Input : self
# Output : None
# Catch : None
def show_raw_data(self):
    label = ""
    for key in self.data.keys():
        if key == "Raw Data":
            label = key
            break
    if self.b_raw_data == False:
        self.b_raw_data = True
        self.btn_rawdata.configure(bg = "green")
        for plot in self.rawdata_plot:
            plot.set_visible(self.b_raw_data)
    elif self.b_raw_data == True:
        self.b_raw_data = False
        self.btn_rawdata.configure(bg="red")
        for plot in self.rawdata_plot:
            plot.set_visible(self.b_raw_data)
    else:
        self.b_raw_data = True
        self.btn_rawdata.configure(bg="green")
        x = self.data[label]["x"]
        y = self.data[label]["y"]
        self.rawdata_plot = self.ax.plot(x,y,"o")
    self.chart_type.draw()


# show_resample 
#
# Description
# Toggle the resampled data
#
# Author
# Stefan Quvang Christensen 22/04/2021
#
# Input : self
# Output : None
# Catch : None
def show_resample(self):
    label = ""
    for key in self.data.keys():
        if key == "Jackknife":
            label = key
            break
    if self.b_resample == False:
        self.b_resample = True
        self.btn_resample.configure(bg="green")
        self.resample_plot.set_visible(self.b_resample)
    elif self.b_resample == True:
        self.b_resample = False
        self.btn_resample.configure(bg="red")
        self.resample_plot.set_visible(self.b_resample)
    else:
        self.b_resample = True
        self.btn_resample.configure(bg="green")
        x = self.data[label]["x"]
        y = self.data[label]["y"]
        self.resample_plot, = self.ax.plot(x,y,"o")
    self.chart_type.draw()


# show_curve_fit 
#
# Description
# Toggle the curve_fit data
#
# Author
# Stefan Quvang Christense 22/04/2021
#
# Input : self
# Output : none
# Catch : none
def show_curve_fit(self):
    label = ""
    for key in self.data.keys():
        if key == "Curve fit":
             label = key
             break
    if self.b_curve_fit == False:
        self.b_curve_fit = True
        self.btn_curvefit.configure(bg="green")
        self.curve_plot.set_visible(self.b_curve_fit)
    elif self.b_curve_fit == True:
        self.b_curve_fit = False
        self.btn_curvefit.configure(bg="red")
        self.curve_plot.set_visible(self.b_curve_fit)
    else:
        self.b_curve_fit = True
        self.btn_curvefit.configure(bg="green")
        x = self.data[label]["x"]
        y = self.data[label]["y"]
        self.curve_plot, = self.ax.plot(x,y,label=label)
    self.chart_type.draw()


# show_sigma 
#
# Description
# Toggle the sigma/error graph
#
# Author
# Stefan Quvang Christensen 22/04/2021
#
# Input : self
# Output : none
# Catch : none
def show_sigma(self):
    label = ""
    for key in self.data.keys():
        if  key == "Error bar":
            label = key
            break
    if  self.b_sigma == False:
        self.b_sigma = True
        self.btn_sigma.configure(bg="green")
        for plot in self.sigma_plot:
            plot.set_visible(self.b_sigma)
    elif self.b_sigma == True:
        self.b_sigma = False
        self.btn_sigma.configure(bg="red")
        for plot in self.sigma_plot:
            plot.set_visible(self.b_sigma)
    else:
        self.b_sigma = True
        self.btn_sigma.configure(bg="green")
        x = self.data[label]["x"]
        y = self.data[label]["y"]
        std = self.data[label]["std"]
        plot = self.ax.errorbar(x=x,y=y,yerr=std,capsize=5)
        plot[0].set_visible(False)
        self.sigma_plot = plot[1] + plot[2]
    self.chart_type.draw()


# check_update 
#
# Description
# start the gui
#
# Author
# Stefan Quvang Christensen 21/04/2021
#
# Input : self
# Output : none
# Catch : none
def check_update(self):
    if not self.q_variables.empty():
        self.update_stat(self.q_variables.get())
    if  not self.q_data.empty():
        self.chart_type.get_tk_widget().delete("all")
        figure = plt.Figure(figsize=(6,5),dpi=100)
        self.ax = figure.add_subplot(111)
        self.chart_type = FigureCanvasTkAgg(figure,self.graph)
        self.chart_type.get_tk_widget().grid(column=0,row=0)
        self.data = self.q_data.get()
        self.ax.grid(True)
        self.b_curve_fit = ""
        self.b_sigma = ""
        self.b_raw_data = ""
        self.b_resample = ""
        self.b_effective = ""
        self.b_effective_error = ""
    if  not self.q_error.empty():
        error = self.q_error.get()
        self.error_text.insert(tk.INSERT,error)
        self.error_text.insert(tk.INSERT,"\n")
    # This is the recursive calls to it self. 
    # So everything in this function must be aboce this
    self.Master.after(200,self.check_update)


# add_fit 
#
# Description
# Add the fitting information
#
# Author
# Stefan Quvang Christensen 21-04-2021
#
# Input : self
# Output : none
# Catch : none
def add_fit(self):
    fit = [self.entry_start.get(),self.entry_end.get()]
    if self.q_filename.empty():
        self.q_filename.put(self.filename)
    self.q_fit.put(fit)


# update_stat 
#
# Description
# Update the statistics of the system. This function should be synchronized to the data thread. 
#
# Author
# Stefan Quvang Christensen 21-04-2021
#
# Input : self
#           variables : list : List of all the variables calculated witin the data module   
# Output : none
# Catch : none
def update_stat(self,variables):
    self.stat_chi_text.set(variables[0])
    self.stat_chi_norm_text.set(variables[1])
    self.stat_chi_red_text.set(variables[2])
    self.stat_sigma_data_text.set(variables[3])
    self.m1_value.set(variables[4])
    self.m1_sigma.set(variables[5])
    self.m2_value.set(variables[6])
    self.m2_sigma.set(variables[7])
    self.z1_value.set(variables[8])
    self.z1_sigma.set(variables[9])
    self.z2_value.set(variables[10])
    self.z2_sigma.set(variables[11])


# UpdateTable 
#
# Description
# Update particle table with new information
#
# Author
# Stefan Quvang Christensen 21-04-2021
#
# Input : self
# Output : None
# Catch : None
def UpdateTable(self):
    particlename_index_start = self.filename.find(".")
    particlename_index_end = self.filename.find(".",particlename_index_start+1)
    particle_name = self.filename[particlename_index_start+1:particlename_index_end]
    quarks_index = self.filename.rfind(".",-5)
    quarks_name= self.filename[quarks_index+1:quarks_index+6]
    self.particlename.set(particle_name)
    self.quarksname.set(quarks_name)


def get_variable(self):
    self.filename = self.variable.get()
    self.UpdateTable()
    self.q_filename.put(self.filename)
    if not self.filename in self.loaded_file:
        self.loaded_file.append(self.filename)
        self.loadfile['values'] = self.loaded_file
        mkdir(self.filename)
