# GUI
#
# Description
# Class for handling the graphical interface. 
# This only contruct the graphical hanlder, but the design lies within implementation files. 
#
# Dependencies
# tkinter
#
# Author and time (DD/MM/YYY)
# Stefan Quvang 20/04/2021
class GUI:
    from ._Layout import build_gui, get_variable, UpdateTable, update_stat, add_fit,check_update, show_curve_fit,show_sigma, show_resample,show_raw_data, select_file, show_effective, show_effective_error, convert_latex, convert_data,add_option, update_file_fit
    def __init__(self,filename,fit,variables,data, error, latex):
        self.q_filename = filename
        self.q_data = data
        self.q_latex = latex
        self.q_fit = fit
        self.q_variables = variables
        self.q_error = error
        self.b_curve_fit = ""
        self.b_sigma = ""
        self.b_raw_data = ""
        self.b_resample = ""
        self.b_effective = ""
        self.b_effective_error = ""
        self.build_gui()
