# This is the first year project
# Written by Stefan Quvang

# Import some basic modules
from thread.thread_data import DataThread
from thread.thread_latex import LatexThread
from GUI import GUI
import queue

# Main Function
# 
# Description : 
# Main function to calculate the mass of a particle
# 
# Input : None
# Output : None
# Catch : None
if __name__ == "__main__":
    # Define some queues
    q_filename = queue.Queue()
    q_fit = queue.Queue()
    q_variables = queue.Queue()
    q_data = queue.Queue()
    q_error = queue.Queue()
    q_latex = queue.Queue()
    # Get the thread object
    t_data = DataThread(1,"Data Thread",q_variables,q_filename,q_fit,q_data,q_error)
    t_latex = LatexThread(thread_id=2,thread_name="LaTex Thread",q_latex=q_latex, q_error=q_error)

    # Start each thread
    t_data.start()
    t_latex.start()

    # Run the Main thread aka the GUI (tkinter is not thread safe, HuH)
    GUI(q_filename,q_fit,q_variables,q_data,q_error,q_latex)

    t_data.stop()
    t_latex.stop()
