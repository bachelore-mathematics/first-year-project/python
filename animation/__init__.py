from manim import *
import numpy as np
import glob
import ast


# Text
#
# Description
# Create introductional text 
#
# Dependencies
# manim
#
# Author and time (DD/MM/YYY)
# Stefan Quvang Christensen 26.05.21
class IntroText(Scene):
    # construct 
    #
    # Description
    # Construct the animation
    #
    # Author
    # Stefan Quvang Christensen 26.05.21
    #
    # Input : self
    # Output : none
    # Catch : none
    def construct(self):
        text_group=Text("Group 1")
        text_group.shift(3*UP)
        self.play(
                Write(text_group),
                )
        text_project = Text("Monte Carlo Simulation")
        text_project.shift(2*UP)
        self.play(Write(text_project))
        text_project2 = Text("of the Strong nuclear Force")
        text_project2.shift(1*UP)
        self.play(Write(text_project2))
        text_thanks=Text("Thanks to our supervisor")
        text_thanks.next_to(text_project2,DOWN)
        text_benja=Text("Assistant Professor Benjamin Jäger")
        text_benja.next_to(text_project2,DOWN)
        self.play(Write(text_thanks))
        self.play(Transform(text_thanks,text_benja))
        text_made_by = Text("Made By :")
        text_made_by.shift(1*DOWN)
        self.play(Write(text_made_by))

        list_members = ["Melissa","Jakob","Christian","Thomas","Bjørn","Stefan"]
        i = 0
        int_pos = 3
        for str_member in list_members :
            text_member = Text(str_member,size=0.7)
            if str_member !=  "Melissa" and (i % 2) != 0:
                self.play(
                        ApplyMethod(text_member.shift,3*DOWN,(i-int_pos)*LEFT))
                int_pos = int_pos - 1
            elif str_member == "Melissa":
                self.play(ApplyMethod(text_member.shift,2*DOWN,(i-int_pos-1)*LEFT))
            else:
                self.play(ApplyMethod(text_member.shift,2*DOWN,(i-int_pos)*LEFT))
            i = i + 1
        self.wait(2)
        # Dummy wait for easy editing
        self.wait(5)


# Jackknife
#
# Description
# Animation for the jackknife
#
# Dependencies
# numpy
# manim
#
# Author and time (DD/MM/YYY)
# stefan quvang today
class  Jackknife(Scene):

    #  construct 
    #
    # Description
    # construct
    #
    # Author
    # stefan quvang today
    #
    # Input : non
    # Output : no
    # Catch : no
    def  construct(self) :
        # Getting index and file

        text_jackknife = Text("Jackknife")
        mathtex_jackknife = MathTex(r'J_i(t) = ', r'\frac{1}{n-1}' , r'\sum_{j=0,j \neq i}^{n}', r' C_j(t)')
        self.add(text_jackknife)
        self.wait(2)
        self.play(Transform(text_jackknife,mathtex_jackknife))
        self.wait(1)
        self.play(ApplyMethod(text_jackknife.shift,3*UP))
        list_data = range(1,10,1)
        dict_object = dict()
        int_index_max = len(list_data)
        int_number_of_col = round(int_index_max/4)
        int_row = 0
        int_col = int_number_of_col
        index = 0
        for float_data in list_data:
            dict_object[index] = Text(str(np.float64(float_data)))
            dict_object[index].shift(int_row*DOWN,int_col*LEFT)
            if int_col == -int_number_of_col:
                int_row = int_row + 1
                int_col = int_number_of_col
            else:
                int_col = int_col - 2
            index = index + 1
        box_cor = SurroundingRectangle(mathtex_jackknife[3],buff=.1)
        box_cor.shift(3*UP)
        group_cor = Group()
        for key in dict_object.keys():
            group_cor.add(dict_object[key])


        self.play(
                Create(box_cor),
                FadeIn(group_cor)
                )

        group_mathtex = Group()
        group_mathtex.add(mathtex_jackknife[2])
        group_mathtex.add(mathtex_jackknife[3])
        box_sum = SurroundingRectangle(group_mathtex)
        box_jack = SurroundingRectangle(mathtex_jackknife[1],buff=.1)
        box_jack.shift(3*UP)
        box_sum.shift(3*UP)
        circle = Circle(color=WHITE)
        circle.scale(0.5)
        circle.set_fill(opacity=0)
        circle.shift(int_number_of_col*LEFT)
        self.play(
                ReplacementTransform(box_cor,box_sum),
                Transform(dict_object[0],circle))
        float_curr_val = 0
        i = 0
        for key in dict_object.keys():
            if i == 0:
                i = 1
                pass
            else:
                self.play(
                        FadeOutAndShift(dict_object[key],circle.get_center() - dict_object[key].get_center()),
                        dict_object[0].animate.scale(1.1)
                        )
        int_sum = 0
        for i in range(1,len(list_data)):
            int_sum = int_sum + list_data[i]
        float_avg = int_sum/(len(list_data)-1)
        text_jack_value = Text(str(float_avg))
        text_jack_value.scale(2)
        text_jack_value.shift(2*LEFT)
        self.wait(0.5)
        self.play(
                ReplacementTransform(box_sum,box_jack),
                Transform(dict_object[0],text_jack_value))
        # Dummy wait for easy editing
        self.wait(5)


# ani_curvefit
#
# Description
# Animation for handling the curve fitting animation
#
# Dependencies
# manim
#
# Author and time (DD/MM/YYY)
# Stefan Quvang Christensen 27.05.21
class ani_curvefit(GraphScene):

     # get_particle                                                                                                          
     #
     # Description
     # Function asking for particle and fit
     #
     # Author
     # Stefan Quvang Christensen 25.05.21 
     #
     # Input : Self
     # Output : none
     # Catch : none
     def get_particle(self):
        list_dir_particle = glob.glob("../Gen*")
        list_particle = list()
        list_fit = list()
        i = 0
        for key in self.dict_particle.keys():
            list_particle.append(self.dict_particle[list_dir_particle[i][1+list_dir_particle[i].find(".",-5):]])
            i = i +1

        for i in range(len(list_particle)):
            print(str(i) + " : " + list_particle[i])

        int_particle = len(list_particle)*2
        while int_particle >= len(list_particle) :
            int_particle = int(input("Select a paticle from 0-" + str(len(list_particle)-1) + " :" ))

        list_dir_fit = glob.glob(list_dir_particle[int_particle] + "/*")
        for i in range(len(list_dir_fit)):
            list_fit.append(list_dir_fit[i][list_dir_fit[i].find("/",-7)+1:])

        for i in range(len(list_fit)):
            print(str(i) + " : " + list_fit[i])

        int_fit = len(list_fit)*2
        while int_fit >= len(list_fit) :
            int_fit = int(input("Select a fit from 0-"  + str(len(list_fit)-1) + " :"))
        str_fit = str(int_fit)
        str_particle = str(int_particle)

        list_data = glob.glob(list_dir_fit[int_fit] + "/*")

        dict_datafile = dict()
        for i in range(len(list_data)):
            dict_datafile[ list_data[i][list_data[i].rfind("/") +1 :] ] = i

        dict_data = dict()
        with open(list_data[dict_datafile["Calculated data"]],"r") as file:
            dict_data = ast.literal_eval(file.read())
        self.m1 = dict_data["m1"]
        self.m2 = dict_data["m2"]
        self.z1 = dict_data["z1"]
        self.z2 = dict_data["z2"]


        dict_jackknife = dict()
        with open(list_data[dict_datafile["Jackknife"]], "r") as file:
            dict_jackknife = ast.literal_eval(file.read())

        self.x = dict_jackknife["x"]
        self.y = dict_jackknife["y"]


     # construct 
     #
     # Description
     # Construction for the animation class
     #
     # Author
     # Stefan Quvang Christensen 27.05.21 
     #
     # Input : self
     # Output : none 
     # Catch : none
     def construct(self):
       self.str_particle = ""
       self.str_fit = ""
       self.dict_particle = {  "uud" : "proton",
                               "sss" : "omega",
                               "uu" : "pion",
                               "ccc" : "tripple charmed omega"
                            }
       self.z1 = 1
       self.z2 = 1
       self.m1 = 1
       self.m2 = 1
       self.get_particle()

       text_curve_fit = Text("Curve Fitting")
       self.play(Write(text_curve_fit))
       self.wait(3)

       GraphScene.__init__(
                self,
                x_max=max(self.x),
                y_min=min(self.y),
                y_max=max(self.y),
                axes_color = BLUE,
                x_axis_label = "Time [a]",
                y_axis_label = "$C(t)$"
               )
       text_curve_fit = Text("Curve Fitting")
       self.play(Write(text_curve_fit))
       self.wait(1)
       mathtex_curve_fit = MathTex(r'C(t) = ' + r'z_1e^{-m_1t}' + r'+' + r'z_2e^{-m_2(128-t)}')
       mathtex_curve_fit.shift(3*UP,3*RIGHT)
       self.play(Transform(text_curve_fit,mathtex_curve_fit))
       self.setup_axes(animate=True)
       group_dot = Group()
       for i in range(len(self.y)):
            dot = Dot(color=RED).move_to(self.coords_to_point(self.x[i], self.y[i]))
            #self.add(dot)
            group_dot.add(dot)
       func_graph = self.get_graph(lambda x :  self.z1*np.exp(-self.m1*x) + self.z2*np.exp(-self.m2*(128-x)),color=GREEN_SCREEN, x_min=min(self.x))
       self.play(FadeIn(group_dot))
       self.play(
               #ShowIncreasingSubsets(group_dot,rate_func=rate_functions.ease_in_sine,run_time=8),
               Create(func_graph,rate_func=rate_functions.ease_in_sine,run_time=9)
               )
       # Dummy wait for easy clipping
       self.wait(5)

# ani_quarks
#
# Description
# This is the animation for the quarks interaction
#
# Dependencies
# manim
#
# Author and time (DD/MM/YYY)
# Stefan Quvang Christensen 30.05.21
class ani_quarks(Scene):
    # construct 
    #
    # Description
    # This method construct the animation
    #
    # Author
    # Stefan Quvang Christensen 30.05.21
    #
    # Input : self
    # Output : non
    # Catch : non
    def construct(self):
        text_proton = Text("Proton")
        text_proton.shift(3.37*UP) 
        self.play(Write(text_proton))
        U = "U"
        circle_up1 = Circle(color=BLUE,name=U, fill_opacity=1)
        text_up1 = Text("U")
        group_up1 = Group()
        group_up1.add(circle_up1)
        group_up1.add(text_up1)
        group_up1.shift(np.sqrt(2.3225)*UP)

        circle_up2 = Circle(color=RED, name = "U", fill_opacity=1)
        text_up2 = Text(U)
        group_up2 = Group()
        group_up2.add(circle_up2,text_up2)
        group_up2.shift(1*DOWN, 1.15*RIGHT)

        circle_down = Circle(color=GREEN, name = "D", fill_opacity=1)
        text_down = Text("D")
        group_down = Group()
        group_down.add(circle_down,text_down)
        group_down.shift(1*DOWN,1.15*LEFT)

        circle_proton = Circle(color=WHITE)
        circle_proton.scale(2.7)
        self.play(FadeIn(circle_proton))

        for i in range(5):
              self.play(
                      CyclicReplace(group_up1,group_up2,group_down,run_time = 2))
        self.wait(5)

