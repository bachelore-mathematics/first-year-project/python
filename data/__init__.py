from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
import statistics
import json
import os
from io import StringIO
import sys

# data
#
# Description
# Class to handle data manipulation
#
# Dependencies
# numpy, scipy and matplotlib
#
# Author and time /DD/MM/YYY
# Stefan Quvang Christensen 20/04/2021
class data:
    def __init__(self,q_variables,filename,fit,data, error):
        sys.stderr = StringIO()
        self.q_data = data
        self.q_filename = filename
        self.q_variable = q_variables
        self.q_fit = fit
        self.q_error = error


    # calculate 
    #
    # Description
    # Calulate data
    #
    # Author
    # Stefan Quvang Christensen 03/05/2021
    #
    # Input : self
    # Output : none
    # Catch : none
    def calculate(self):
        tupple_fit =  self.q_fit.get()
        if tupple_fit == "exit":
            return None
        string_filename = self.q_filename.get()
        # Define variable 

        string_path = "datafiles/" + string_filename # Store Path to file in a string
        list_labels = ["Raw Data","Curve fit"] # Set legend labels for multiple plot
        list_style = ['.','-'] # Set plot style for each plot. See Matplotlib for documentation on line styles, markers and colors. 
        int_data_points = 128 # Number of datapoints in the set. This value correspond to number of x values. 
        int_data_set = 0 # The size of the data set from the file. 
        int_variable_number = 4 # Number of unknown variables in the basis function
        int_start = int(tupple_fit[0]) # Start of the fittet data
        int_end = int(tupple_fit[1]) # End of the fittet data
        int_fit = int_end - int_start # The fit range of data

        string_new_path = string_filename + "/" + str(int_start) + "-" + str(int_end)

        if os.path.isdir(string_new_path):
            raise Exception("Data already calculated for this file and fitting")

        # Fetching data from file and return two variables, the x and y data
        xin,yin = self.open_file(string_path) # Open file and load data into x,y

        # Get the size of the data set
        int_data_set = np.count_nonzero(xin == 1) # Get number of simulation
        int_data_points = int(len(xin)/int_data_set) # Get number of data point in each simulation

        if int_data_points < int_end:
            int_end = int_data_points

        # Data processing
        list_xdata_org,list_ydata_org = self.data_structure([xin,yin],int_data_points) # Get the stuctured data

        list_ydata_fit = list_ydata_org[int_start:int_end] # Fit ydata

        list_avg_y = self.average(list_ydata_org) # Calculate the average
        list_avg_y_fit = list_avg_y[int_start:int_end] # Fit the average ydata
        list_jackknife = self.jackknife(list_avg_y,list_ydata_org) # Resample using jackknife
        list_jackknife_avg = self.average(list_jackknife) # Calculate the average of the jackknife data

        list_jackknife_fit = list_jackknife[int_start:int_end] # Fitting the jackknife data
        list_jackknife_avg_fit = self.average(list_jackknife_fit) # Getting the average of the fittet jackknife

        std_dev, var = self.std_deviation(list_jackknife_avg,list_jackknife,True) # Calculate standard deviation and the variance
        std_dev = std_dev[int_start:int_end]
        var = var[int_start:int_end]
        t = xin[int_start:int_end] # Fit the xdata

        # Cure fitting processing.
        ft = [0]*len(t) # Curve fitting data, prepare variable to hold function output. 

        # Using the original dataset to determine a really good initial geuss. 
        init_geuss,pcovy = curve_fit(self.bases_func,t,list_avg_y_fit)
        
        self.q_error.put(sys.stderr.getvalue().strip())

        popt = np.zeros((int_data_set,int_variable_number)) # Prepare variables for the estimated variables 
        pcov = np.zeros((int_data_set,int_variable_number,int_variable_number)) # Prepare variables for the pcov matrix

        for i in range(int_data_set):
            popt[i],pcov[i] = curve_fit(self.bases_func,t, list_jackknife_fit[:,i],init_geuss,sigma=std_dev) # Curve Fitting for all jackknife data
        self.q_error.put(sys.stderr.getvalue().strip())
        
        # Reorganise the popt variables
        list_variable = np.zeros((int_variable_number,int_data_set))
        for i in range(int_variable_number):
            list_variable[i] = popt[:,i]

        z1,m1,z2,m2 = self.average(list_variable) # Get the average z and m data out from the popt variable. 
        ft = self.bases_func(t,z1,m1,z2,m2) # Use the calculated data to calculate and populate the ft variable.
        int_m_std_dev, int_m_var = self.std_deviation(m1,list_variable[1],False) # Calculate the standard deviation for the variables
        int_m2_std_dev, int_tmp = self.std_deviation(m2,list_variable[3],False)
        int_z1_std_dev, int_tmp = self.std_deviation(z1,list_variable[0],False)
        int_z2_std_dev, int_tmp = self.std_deviation(z2,list_variable[2],False)

        # Effective mass calculation
        list_mff = np.zeros((int_fit-1,int_data_set))
        for k1 in range(int_data_set):
            for t1 in range(int_fit-1):
                list_mff[t1][k1] = abs(np.log(list_jackknife_fit[t1][k1] / list_jackknife_fit[t1+1][k1]))
        
        list_mavg = self.average(list_mff)
        list_msigma, list_mvar = self.std_deviation([m1]*len(list_mavg.tolist()),list_mff,False)

        


        # Error Calculation
        error, normi, chi = self.chi_square(ft,list_jackknife_avg_fit,std_dev) # Input the Calculated ft and the original data set into the chi_square function
        keys = ["Chis","Chin","Chir","Error","m1","Error m1","m2","Error m2","z1","Error z1","z2","Error z2"]
        variables = [error,normi,chi,std_dev.mean(),m1,int_m_std_dev,m2,int_m2_std_dev,z1,int_z1_std_dev,z2,int_z2_std_dev]
        self.q_variable.put(variables)

        # Create a dict
        data = {    "Raw Data":{"x": list_xdata_org.tolist(), "y":list_ydata_org.tolist()},
                    "Jackknife":{"x": t.tolist(),"y":list_jackknife_avg_fit.tolist()},
                    "Curve fit":{"x": t.tolist(),"y":ft.tolist()},
                    "Error bar":{"x": t.tolist(),"y":list_jackknife_avg_fit.tolist(),"std":std_dev.tolist()},
                    "Effective Mass":{"x":list(range(len(list_mavg.tolist()))),"y":list_mavg.tolist(),"m1":[m1]*len(list_mavg.tolist())},
                    "Sigma meff":{"x":list(range(len(list_mavg))),"y":list_mavg.tolist(), "std":list_msigma.tolist()},
                    "Calculated data":{}
                }
        i = 0
        for key in keys:
            data["Calculated data"][key] = variables[i].tolist()
            i = i +1

        self.q_data.put(data)
        # Write dictionary to file
        string_new_path = string_filename + "/" + str(int_start) + "-" + str(int_end)
        os.mkdir(string_new_path)
        for key in data.keys():
            with open(string_new_path + "/" +  key,"w") as f:
                f.write(json.dumps(data[key]))

    # open_file 
    #
    # Description
    # Open a file specified by string_filename
    #
    # Input : string_filename, string indicating the path to the file to open
    # Output : x,y data from the file as a tuple
    # Catch : none
    def open_file(self,string_filename):
        # Open file 
        string_data = open(string_filename,'r')
    
        # Load data from file into variable
        tuple_data = np.loadtxt(string_data,skiprows=1)
        string_data.close()
        # Load data into x and y variables
        x = tuple_data[:,0]
        y = tuple_data[:,1]
        return x,y
    
    
    # plot_data 
    #
    # Description
    # Plot the data from the input into a traditional 2d plot
    #
    # Input :   x, comma seperated list of x values to be plotted 
    #           y, comma seperated list of y, values to be plotted
    #           labels="", comma seperated list of the labels of each plot, default empty
    #           plot=[], list of string, comma seperated string indicating the plot type. See matplotlib for different markers, linestyle and colors. 
    # Output : none
    # Catch : none
    def plot_data(self,x,y,labels="",style=[]):
        # Setup some basic plot configuration
        plt.grid(True)
        plt.xlabel("Time [t]")
        plt.ylabel("Correlation function C(t)")
    
        plot=np.zeros(len(x))
    
        if len(plot) == 0:
            for i in range(len(x)):
                style[i] = "-"
    
        # Plot the different functions 
        for i in range(len(x)):
            plt.plot(x[i],y[i],style[i])
    
        # A bit more configuration and show the plot
        plt.figlegend(labels=labels)
        plt.show()
    
    # bases_func 
    #
    # Description
    # This is the basis function for the curve_fit function call from scipy. 
    # This function which function the curve_fit should use in its calculation
    #
    # Input :   t  : list[Float]. Variable, x-value of our system
    #           z1 : Float. Constant value, needs to be determined  
    #           m1 : Float. Constant value, needs to be determined
    #           z2 : Float. Constant value, needs to be determined
    #           m2 : Float. Constant value, needs to be determined
    # Output : f(t) : Float. Variable, y-value of our system 
    # Catch : None
    def bases_func(self,t,z1,m1,z2,m2 ):
        return z1*np.exp(-m1*t) + z2*np.exp(-m2*(128-t)) # Return the result from the given function.
    
    # chi_square 
    #
    # Description
    # Calculate the chi_square value to determine the error. 
    # Take the difference between the calculated ft and the data f. 
    # Square the difference and add it to chi^2
    # Repeat until all difference between ft and f is calculated. 
    #
    # Input : ft : array(float), The output from the true function bases_func
    #         f : array(float), The original data
    #         sigma : list[float], defualt = [], The list of sigam values
    # Output : chi_square : The chi^2 value
    #          chi_norm : The chi^2 normalized value
    #          chi : The normalized chi divided by n-4
    # Catch : None
    def chi_square(self,ft,f, sigma = []):
        if len(sigma) == 0:
            print("Empty")
            sigma = np.ones(len(ft)) # Here we avoid divide by zero by initialize all values in the list to 1. 
    
        chi_square = 0 # Initialize value, starting condition/expected is that there are no error :D 
        chi_norm = 0 # Initialize value, starting condition/expected is that there are no nomalized error. 
        for i in range(len(ft)): # For Loop :D 
            chi_square = chi_square + (ft[i] - f[i])**2 # Implement the chi^2 algorithm. 
            chi_norm = chi_norm + ((ft[i]-f[i]) / ( sigma[i]))**2
        chi = chi_norm / (len(ft)-4)
    
        return chi_square,chi_norm,chi # Return the final value
    
    
    # data_structure 
    #
    # Description
    # Take a some data make a structure out of it, and return a structured matrix
    #
    # Input : data, list[list]], The data red from file. 
    #         size, The size of each list in the output matrix
    # Output : xmat, list[list[]], List containing the structured x data  
    #          ymat, list[list], list containing the y data
    # Catch : none
    def data_structure(self,data,size):
        n = int(len(data[0])/size)
        xmat = np.zeros((size,n))
        ymat = np.zeros((size,n))
        for i in range(n):
             for j in range(size):
                 xmat[j][i] = data[0][i*size + j]
                 ymat[j][i] = data[1][i*size + j]
        return xmat,ymat
    
    
    # Average 
    #
    # Description
    # Calculate the average of given data
    #
    # Input : data, list, list of the data to be average
    # Output : avg, list, of the average data
    # Catch : non
    def average(self,data):
        size = len(data)
        elements = len(data[0])
        avg = np.zeros(size)
        for i in range(size):
            for j in range(elements):
                avg[i] = avg[i] + data[i][j]
            avg[i] = avg[i] / elements
        return avg
    
    
    # jackknife 
    #
    # Description
    # Use jackknife for resampling of data points. 
    #
    # Input :   avg, list, list contains all the avg data 
    #           data, list, list of the data to be jackknifed. 
    # Output : jk, list,the jackknife data
    # Catch : none
    def jackknife(self,avg,data):
        size = len(data)
        elements = len(data[0])
        jk = np.zeros((size,elements))
        for i in range(size):
            for j in range(elements):
                jk[i][j] = (elements*avg[i]-data[i][j])/(elements - 1)
        return jk
    
    
    # std_deviation 
    #
    # Description
    # Calculates the standard deviation
    #
    # Input : avg : list[float] : List of floats containing the avg
    #         val : list[list] : List of all the values which the avg from the input
    #         jackknife : bool : True for jackknife std dev, false for normal. 
    # Output : std_dev : list[float] : list of all the standard deviation
    #          var : list[float] : list of all the variance
    # Catch : TypeError : catch typeerror when dealing with array instead of matrix
    def std_deviation(self,avg, val,jackknife):
        size = len(val)
        try :
            elements = len(val[0])
            var = np.zeros(size)
            if jackknife:
                for i in range(size):
                    for j in range(elements):
                        var[i] = var[i] + (val[i][j]-avg[i])**2*(elements-1)/elements
            else:
                for i in range(size):
                    for j in range(elements):
                        var[i] = var[i] + (val[i][j]-avg[i])**2/(elements - 1)
            std_dev = np.sqrt(var)
        except TypeError:
            var = 0
            for i in range(size):
                var = var + (val[i]-avg)**2*(size-1)/(size)
            std_dev = np.sqrt(var)
        except Exception as error:
            print(error)
        finally:
            return std_dev, var
