import matplotlib.pyplot as plt
import matplotlib
from os import mkdir, remove
from os.path import isdir, isfile
import json
import numpy as np

# latex
#
# Description
# Class to handle the convertion to latex files
#
# Dependencies
# file, matplotlib, os
#
# Author and time (DD/MM/YYY)
# Stefan Quvang 27/04/2021
class latex:
    def __init__(self,latex):
        # Configure matplotlib to make nice svg plot for latex
        matplotlib.use("pgf")
        matplotlib.rcParams.update({
                "pgf.texsystem": "lualatex", #xdflatex",
                'font.family': 'serif',
                'text.usetex': True,
                'pgf.rcfonts': False,
                })
        # Initialize stuff
        self.q_latex = latex
        self.latex_path = "latex/output/"
        self.ylabel = ["C(t)",r'Effective Mass']
        self.xlabel = r'time [a]'
        self.particle = [["Proton","0","0","0","0"]]
        self.data = {   "m1": [0,0],
                        "m2": [1,1],
                        "z1": [2,2],
                        "z2": [3,3]}

    # start 
    #
    # Description
    # Start the convertion
    #
    # Author
    # Stefan Quvang Christensen 03/05/2021
    #
    # Input : self
    # Output : none
    # Catch : none
    def start(self):
        tmp_dict = self.q_latex.get()
        if  tmp_dict == "exit":
            return None
        self.multiplot = tmp_dict["graph"]
        self.file = tmp_dict["file"]
        self.fit = str(tmp_dict["fit"]["start"]) + "-" + str(tmp_dict["fit"]["end"])
        self.graph = list()
        for key in self.multiplot.keys():
            self.graph.append(key)
        with open(self.file + "/" + self.fit + "/" + "Calculated data","r") as f:
            dict_tmp_data = json.load(f)
        list_keys = list(dict_tmp_data.keys())
        for i in range(1,len(self.particle[0])):
            self.particle[0][i] = str(np.format_float_scientific(np.float(dict_tmp_data[list_keys[i-1]]),4))
        particlename_index_start = self.file.find(".")
        particlename_index_end = self.file.find(".",particlename_index_start+1)
        particle_name = self.file[particlename_index_start+1:particlename_index_end]
        self.particle[0][0] = particle_name
        b_get_data = False
        list_variables = ["m1","z1","m2","z2"]
        var_index = 0
        index = 0
        for key in dict_tmp_data.keys():
            if b_get_data :
                data = np.float( dict_tmp_data[key])
                data = np.format_float_scientific(data,4)
                self.data[list_variables[var_index]][index] = data
                var_index = var_index + index
                index = not index
            if key == "Error":
                b_get_data = True
        tmp = ["Curve fit", "Jackknife","Error bar","Effective Mass","Raw Data","Sigma meff"]
        plot_config = ["-", "o", "-", "o", "o", "-"]
        self.dict_plot_config = dict()
        for i in range(len(tmp)):
            self.dict_plot_config[tmp[i]] = plot_config[i]
        if not isdir("latex/output"):
            mkdir("latex/output")
            mkdir("latex/output/images")
        elif not isdir("latex/output/images"):
            mkdir("latex/output/images")
        self.draw_plot()
        self.write_latex()


    # write_latex 
    #
    # Description
    # Write a latex file with the given data
    #
    # Author
    # Stefan Quvang Christensen 27/04/2021
    #
    # Input : self
    # Output : none
    # Catch : none
    def write_latex(self):
        f_latex = self.latex_path + self.file + ".tex"
        if isfile(f_latex):
             remove(f_latex)
        with open(f_latex,"a") as f:
            f.write(r'\subsection{Analyse of ' + self.particle[0][0] + r'}' + "\n" +
                    r'By using the describe mathematical statitics and resampling technique, the data in table~\ref{tab:' +
                    self.file + r'-data} can be calculated for ' + self.particle[0][0] + r': \\' + "\n"
                    r'\begin{table}[H]' + "\n" +
                    r' \centering' + "\n" +
                    r'\begin{tabular}{ccccc}' + "\n" +
                    r'\toprule' + "\n" +
                    r'& $\chi^2$ & Reduced $\chi$ & Normalized $\chi$ & $\sigma$ \\' + "\n")
            for i in range(len(self.particle)):
                f.write( r'\midrule' + "\n" +
                    self.particle[i][0] + r' & $' + self.particle[i][1] +  r'$ &$' +  self.particle[i][2] +  r'$&$' +  self.particle[i][3] + r'$&$'  + self.particle[i][4] + r'$\\' + "\n")
            f.write(r'\bottomrule' + "\n" +
                r'\end{tabular}' + "\n" +
                r'\caption{Statistics of the particle}' + self.particle[0][0]  +  "\n" +
                r'\label{tab:' + self.file + r'-data}'  + "\n" +
                r'\end{table} \\' + "\n"
                )
            f.write(r' By taking a deeper look at the curve fitting of the variables ' +
                       r'$m1$, $m2$, $z1$ and $z2$ and estimation can be calculated to table~\ref{tab:' +
                       self.file + r'-estimation}' +
                       r'\begin{table}[H]' + "\n" +
                       r'\centering' + "\n" +
                       r'\begin{tabular}{ccccc}' + "\n" +
                       r'\toprule' + "\n" +
                       r'& Estimation & $\sigma$ \\' + "\n"
                       )
            for key in self.data.keys():
                   f.write(r'\midrule' + "\n" +
                           "$" + key + "$&$" + str(self.data[key][0]) + "$&$" + str(self.data[key][1]) + r'$ \\' + "\n"
                           )
            f.write(r'\bottomrule' + "\n" +
                    r'\end{tabular}' + "\n" +
                    r'\caption{Estimation of mass $m1$, $m2$ and the overlap factor $z1$, $z2$ with $\sigma$}' + "\n" +
                    r'\label{tab:' + self.file + r'-estimation}' + "\n" +
                    r'\end{table}' + "\n" +
                    r'The given data in tables can give an indication of the precision of the estimation, which indicate that MANUAL INPUT. \\ But another way to validate the precision of the data is to analyze the data through plots. The following plots will give an indication of MANUAL INPUT' + "\n")

            for plot in self.graph:
                file_plot = (self.file + "_" + self.fit + "_" + plot).replace(" ","")
                
                f.write(r'\begin{figure}[H]' + "\n"  + r' \begin{center}' + "\n"  + r' \scalebox{0.75}{\input{images/' + file_plot + r'.pgf}}' + "\n" + r' \end{center}' + "\n" + r' \caption{Plot displaying the ' + plot + r' as a function of time with the fitting criteria of ' +self.fit+ r'}' + "\n" + r'\label{fig:' + file_plot + "}"  + "\n"+ r' \end{figure} ')
                f.write("\n")
                f.write(r'As it can be seen in~\ref{fig:' + file_plot  + r'} MANUAL USER INPUT')
                f.write("\n")
                f.write("\n")
        print(r'\input{CHAPTER/SECTION/' + self.file  + "}")


    # draw_plot
    #
    # Description
    # Draw the plot to be printed
    #
    # Author
    # Stefan Quvang Christensen 27/04/2021
    #
    # Input : self
    # Output : none
    # Catch : none
    def draw_plot(self):
        list_keys = self.multiplot.keys()
        index = 0
        self.m1 = 0
        self.avg_std = 0
        for plot in self.graph:
            plt.clf()
            print_file = (self.latex_path + "images/" + self.file + "_" + self.fit + "_" + plot + ".pgf").replace(" ","")
            for f_data in self.multiplot[plot]:
                with open(self.file + "/" + self.fit + "/" + f_data,"r") as f:
                    data = json.load(f)
                    x = data["x"]
                    y = data["y"]
                    if "error" in f_data.lower()  or "sigma" in f_data.lower():
                        std = data["std"]
                        plots = plt.errorbar(x,y,std,capsize=5,label=f_data)
                        plots[0].set_visible(False)
                    elif "effective" in f_data.lower():
                        m1 = data["m1"]
                        self.m1 = m1[0]
                        plot1 = plt.plot(x,y,self.dict_plot_config[f_data],label=f_data)
                        plot2 = plt.plot(x,m1,label="m1")

                    else:
                        plots = plt.plot(x,y,self.dict_plot_config[f_data],label=f_data)
                    plt.ylabel(self.ylabel[index])
                    plt.xlabel(self.xlabel)
            index = index + 1
            if isfile(print_file):
                remove(print_file)
            plt.legend()
            plt.savefig(print_file,dpi=400)
