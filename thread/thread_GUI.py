import threading
import time
from GUI import GUI

# GuiThread
#
# Description
# Create and handle the thread for a GUI
#
# Dependencies
# Threading, time and GUI
#
# Author and time (DD/MM/YYY)
# Stefan Quvang Christensen 20/04/2021
class GuiThread (threading.Thread):
    def __init__(self, thread_id, name, filename,fitting,variables,data,error,latex):
        threading.Thread.__init__(self)
        self.filename = filename
        self.fitting = fitting
        self.q_latex = latex
        self.q_data = data
        self.q_variables = variables
        self.q_error = error
        self.ThreadId = thread_id
        self.Name = name

    # run 
    #
    # Description
    # Activate the thread for GUI
    #
    # Author
    # Stefan Quvang Christensen  20/04/2021
    #
    # Input : self
    # Output : None
    # Catch : None
    def run(self):
        self.gui = GUI(self.filename,self.fitting,self.q_variables,self.q_data,self.q_error,latex=self.q_latex)


    # update_val_stat 
    #
    # Description
    # return gui instance
    #
    # Author
    # Stefan Quvang Christensen 21/04/2021
    #
    # Input :   self
    # Output : none
    # Catch : none
    def update_val_stat(self,variables):
       self.gui.update_stat(variables)
