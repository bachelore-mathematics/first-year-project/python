import threading
import time
from data import data

# thread_data
#
# Description
# The thread to handle the data manipulation
#
# Dependencies
# threading, time and data
#
# Author and time (DD/MM/YYY)
# Stefan Quvang Christensen 20/04/2021
class DataThread (threading.Thread):
    def __init__(self, thread_id,name,q_variables,q_filename,q_fit,q_data,q_error):
        threading.Thread.__init__(self)
        self.ThreadId = thread_id
        self.Name = name
        self.Stop = False
        self.q_fit = q_fit
        self.q_error = q_error
        self.cldata = data(q_variables=q_variables,filename=q_filename,fit=q_fit,data=q_data,error=q_error)

    # run 
    #
    # Description
    # Method for starting the thread
    #
    # Author
    # Stefan Quvang Christensen
    #
    # Input : self
    # Output : none
    # Catch : none
    def run(self):
        while self.Stop == False :
            try:
                self.cldata.calculate()
            except Exception as error:
                self.q_error.put(error)
        del self.cldata
        return None

    # stop 
    #
    # Description
    # Stop the thread from running
    #
    # Author
    # Stefan Quvang Christensen 03/05/2021
    #
    # Input : self
    # Output : none
    # Catch : none
    def stop(self):
       self.Stop = True
       self.q_fit.put("exit")
