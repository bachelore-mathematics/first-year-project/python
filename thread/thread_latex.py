import threading
from latex import latex

# LatexThread
#
# Description
# Thread for handling the latex convertion
#
# Dependencies
# threading, latex
#
# Author and time (DD/MM/YYY)
# Stefan Quvang Christensen 03/05/2021
class LatexThread(threading.Thread):
    def __init__(self,thread_id,thread_name,q_latex,q_error):
        threading.Thread.__init__(self)
        self.ThreadId = thread_id
        self.ThreadName = thread_name
        self.q_latex = q_latex
        self.q_error = q_error
        self.Stop = False
        self.cllatex = latex(self.q_latex)


    # run 
    #
    # Description
    # Run/start the thread
    #
    # Author
    # Stefan Quvang Christensen 03/05/2021
    #
    # Input : self
    # Output : none
    # Catch : none
    def run(self):
        while self.Stop == False :
            try:
                self.cllatex.start()
            except Exception as e:
                if not self.q_error.empty():
                    self.Stop = True
                self.q_error.put(e)
        del self.cllatex

    # stop 
    #
    # Description
    # Stop the thread
    #
    # Author
    # Stefan Quvang Christensen 04/05/2021
    #
    # Input : self
    # Output : none
    # Catch : none
    def stop(self):
        self.Stop = True
        self.q_latex.put("exit")
